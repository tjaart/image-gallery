# Image Gallery #

## Installation ##

[Download](https://gitlab.com/tjaart/image-gallery/-/jobs/artifacts/master/download?job=pages) the compiled artifacts.

Copy `public/elm.js` and `public/index.html` into a directory served by your webserver. 

You need to replace `public/metadata.json` with your own metadata.

## Metadata ##

The metadata is fairly straightforward to update: 

It contains an array of galleries, each having a `title`, `description` and array of `images`. Each image contains a `description`, `title` and `url`.

For Example:

```json
[
  {
    "title": "My Gallery title",
    "description": "My Gallery description",
    "images": [
      {
        "url": "http://my_image_url.com",
        "title": "Photo title 1",
        "description": "Photo description 1"
      },
      ... 
    ]
  },
  
  ...
]


```
