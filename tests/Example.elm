module Example exposing (suite)

import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, int, list, string)
import Test exposing (..)


suite : Test
suite =
    describe "Dummy Test"
        [ test "Test True" (\_ -> Expect.equal True True)
        , test "Test False" (\_ -> Expect.equal False False)
        ]
