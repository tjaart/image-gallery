module Main exposing (Model, Msg(..), main, update, view)

import Array exposing (Array, fromList, get)
import Browser
import Browser.Dom exposing (Viewport, getViewport)
import Browser.Events exposing (onResize)
import Debug exposing (log)
import Element exposing (alignRight, centerX, centerY, column, el, height, html, image, layout, px, rgb, row, text)
import Element.Background exposing (tiled)
import Element.Border exposing (rounded, shadow)
import Element.Events exposing (onClick)
import Element.Input exposing (button)
import Html exposing (Html)
import Http
import Json.Decode exposing (Decoder, field, list, string)
import Json.Decode.Pipeline exposing (hardcoded, optional, required)
import List exposing (isEmpty, length)
import Svg
import Svg.Attributes as SvgAttr
import Task exposing (perform)
import Url.Builder as Url



-- MAIN


main =
    Browser.element
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }



-- MODEL


type alias Model =
    { galleries : List Gallery
    , locator : { gallery : Int, image : Int }
    , windowsize : Windowsize
    }


type alias Gallery =
    { title : String, description : String, images : List Image }


type alias Image =
    { url : String, title : String, description : String }


type alias Windowsize =
    { width : Float, height : Float }


init : () -> ( Model, Cmd Msg )
init flags =
    ( Model [] { gallery = 1, image = 1 } (Windowsize 0 0)
    , Cmd.batch [ getGalleries, updateViewport ]
    )



-- DECODER


getGalleries : Cmd Msg
getGalleries =
    Http.send LoadGalleriesMsg (Http.get (Url.absolute [ "meta.json" ] []) galleriesDecoder)


updateViewport : Cmd Msg
updateViewport =
    Task.perform ViewportMsg getViewport


galleriesDecoder : Decoder (List Gallery)
galleriesDecoder =
    list
        (Json.Decode.succeed Gallery
            |> optional "title" string ""
            |> optional "description" string ""
            |> required "images" imagesDecoder
        )


imagesDecoder : Decoder (List Image)
imagesDecoder =
    list
        (Json.Decode.succeed Image
            |> required "url" string
            |> optional "title" string ""
            |> optional "description" string ""
        )


type Msg
    = LoadGalleriesMsg (Result Http.Error (List Gallery))
    | ViewportMsg Viewport
    | ResizeMsg Int Int
    | PreviousGalleryMsg
    | NextGalleryMsg
    | PreviousImageMsg
    | NextImageMsg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        LoadGalleriesMsg status ->
            case status of
                Ok galleries ->
                    ( { model | galleries = galleries }, Cmd.none )

                Err _ ->
                    ( model, Cmd.none )

        ResizeMsg width height ->
            ( model, updateViewport )

        ViewportMsg viewport ->
            ( { model
                | windowsize =
                    Windowsize viewport.viewport.width viewport.viewport.height
              }
            , Cmd.none
            )

        PreviousImageMsg ->
            if model.locator.image > 1 then
                ( { model
                    | locator =
                        { gallery = model.locator.gallery, image = model.locator.image - 1 }
                  }
                , Cmd.none
                )

            else
                ( model, Cmd.none )

        NextImageMsg ->
            if model.locator.image < imagesLength model then
                ( { model
                    | locator =
                        { gallery = model.locator.gallery, image = model.locator.image + 1 }
                  }
                , Cmd.none
                )

            else
                ( model, Cmd.none )

        PreviousGalleryMsg ->
            ( { model
                | locator =
                    { gallery = model.locator.gallery - 1, image = model.locator.image }
              }
            , Cmd.none
            )

        NextGalleryMsg ->
            ( { model
                | locator =
                    { gallery = model.locator.gallery + 1, image = model.locator.image }
              }
            , Cmd.none
            )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    -- Fire on window resize
    Sub.batch [ onResize ResizeMsg ]



-- VIEW


view : Model -> Html Msg
view model =
    layout []
        (column []
            [ el [] (text (imageTitle model))
            , el [ centerY ]
                (row []
                    [ if model.locator.image > 1 then
                        el [ onClick PreviousImageMsg ] (html backArrow)

                      else
                        el [] (text "")
                    , image
                        [ height (px (round model.windowsize.height - 50))
                        , shadow
                            { offset = ( 3, 3 )
                            , size = 3
                            , blur = 3
                            , color = rgb 0 0 0
                            }
                        ]
                        { src = imageUrl model, description = imageDescription model }
                    , if model.locator.image < imagesLength model then
                        el [ onClick NextImageMsg ] (html forwardArrow)

                      else
                        el [] (text "")
                    ]
                )
            , el [] (text (imageDescription model))
            ]
        )



-- HELPERS


galleriesLength : Model -> Int
galleriesLength model =
    length model.galleries


galleryTitle : Model -> String
galleryTitle model =
    case get (model.locator.gallery - 1) (fromList model.galleries) of
        Just gallery ->
            gallery.title

        Nothing ->
            ""


galleryDescription : Model -> String
galleryDescription model =
    case get (model.locator.gallery - 1) (fromList model.galleries) of
        Just gallery ->
            gallery.description

        Nothing ->
            ""


imagesLength : Model -> Int
imagesLength model =
    case get (model.locator.gallery - 1) (fromList model.galleries) of
        Just gallery ->
            length gallery.images

        Nothing ->
            0


imageTitle : Model -> String
imageTitle model =
    case get (model.locator.gallery - 1) (fromList model.galleries) of
        Just gallery ->
            case get (model.locator.image - 1) (fromList gallery.images) of
                Just image ->
                    image.title

                Nothing ->
                    ""

        Nothing ->
            ""


imageDescription : Model -> String
imageDescription model =
    case get (model.locator.gallery - 1) (fromList model.galleries) of
        Just gallery ->
            case get (model.locator.image - 1) (fromList gallery.images) of
                Just image ->
                    image.description

                Nothing ->
                    ""

        Nothing ->
            ""


imageUrl : Model -> String
imageUrl model =
    case get (model.locator.gallery - 1) (fromList model.galleries) of
        Just gallery ->
            case get (model.locator.image - 1) (fromList gallery.images) of
                Just image ->
                    image.url

                Nothing ->
                    ""

        Nothing ->
            ""


backArrow =
    Svg.svg
        [ SvgAttr.width "120", SvgAttr.height "120", SvgAttr.viewBox "0 0 32 32" ]
        [ Svg.path [ SvgAttr.d "M14.19 16.005l7.869 7.868-2.129 2.129-9.996-9.997L19.937 6.002l2.127 2.129z" ] [] ]


forwardArrow =
    Svg.svg [ SvgAttr.width "120", SvgAttr.height "120", SvgAttr.viewBox "0 0 32 32" ]
        [ Svg.path [ SvgAttr.d "M18.629 15.997l-7.083-7.081L13.462 7l8.997 8.997L13.457 25l-1.916-1.916z" ] [] ]
